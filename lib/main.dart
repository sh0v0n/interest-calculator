import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
        primaryColor: Colors.indigo,
        accentColor: Colors.indigoAccent,
        brightness: Brightness.dark,
    ),
    home: Scaffold(
      appBar: AppBar(
        title: Text("Interest Calculator"),
      ),
      body: Calculator(),
    ),
  ));
}

class Calculator extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CalculatorState();
  }
}

class _CalculatorState extends State<Calculator> {
  String _selectedCurrency;
  List<String> _currencies = ["Taka", "Ruppees", "Dollars", "Other"];
  String _displayText = "";

  @override
  void initState() {
    super.initState();
    this._selectedCurrency = this._currencies[0];
  }

  TextEditingController principalController = TextEditingController();
  TextEditingController roiController = TextEditingController();
  TextEditingController termController = TextEditingController();

  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;

    return Center(
        child: Form(
            key: _formKey,
            child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  children: <Widget>[
                    Image(
                      image: AssetImage("images/calculator.png"),
                      width: 230,
                      height: 230,
                    ),
                    Padding(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          style: textStyle,
                          validator: (String value){
                            if(value.isEmpty){
                              return "Please enter principal amount";
                            }
                            else{
                              try
                              {
                                double.parse(value);
                              }
                              catch (e){
                                return "Only number is acceptable";
                              }
                            }
                          },
                          controller: principalController,
                          decoration: InputDecoration(
                              labelText: "Principal",
                              hintText: "Input principal e.g 12000",
                              labelStyle: textStyle,
                              errorStyle: TextStyle(
                                color: Colors.yellowAccent,
                                fontSize: 18.0
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0))),
                        )),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        style: textStyle,
                        controller: roiController,
                        validator: (String value){
                          if(value.isEmpty){
                            return "Please enter interest rate";
                          }
                          else{
                            try
                            {
                              double.parse(value);
                            }
                            catch (e){
                              return "Only number is acceptable";
                            }
                          }
                        },
                        decoration: InputDecoration(
                            labelText: "Rate of interest",
                            hintText: "In percent",
                            labelStyle: textStyle,
                            errorStyle: TextStyle(
                                color: Colors.yellowAccent,
                                fontSize: 18.0
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0))),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              style: textStyle,
                              controller: termController,
                              validator: (String value){
                                if(value.isEmpty){
                                  return "Please enter year";
                                }
                                else{
                                  try
                                  {
                                    double.parse(value);
                                  }
                                  catch (e){
                                    return "Only number is acceptable";
                                  }
                                }
                              },
                              decoration: InputDecoration(
                                  labelText: "Term",
                                  labelStyle: textStyle,
                                  hintText: "Time in years",
                                  errorStyle: TextStyle(
                                      color: Colors.yellowAccent,
                                      fontSize: 18.0
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.circular(5.0))),
                            ),
                          ),
                          Container(
                            width: 25,
                          ),
                          Expanded(
                            child: DropdownButton<String>(
                              value: _selectedCurrency,
                              items: _currencies.map((String currency) {
                                return DropdownMenuItem<String>(
                                  value: currency,
                                  child: Text(
                                    currency,
                                    style: textStyle,
                                  ),
                                );
                              }).toList(),
                              onChanged: (String selectedCurrency) {
                                setState(() {
                                  this._selectedCurrency = selectedCurrency;
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                color: Theme.of(context).accentColor,
                                textColor: Theme.of(context).primaryColorDark,
                                onPressed: () {
                                  setState(() {
                                    if(_formKey.currentState.validate()){
                                      _displayText = calculateTotalReturn();
                                    }
                                  });
                                },
                                child: Text(
                                  "Calculate",
                                  textScaleFactor: 1.5,
                                ),
                              ),
                            ),
                            Expanded(
                              child: RaisedButton(
                                color: Theme.of(context).primaryColorDark,
                                textColor: Theme.of(context).primaryColorLight,
                                onPressed: () {
                                  setState(() {
                                    resetValues();
                                  });
                                },
                                child: Text(
                                  "Reset",
                                  textScaleFactor: 1.5,
                                ),
                              ),
                            )
                          ],
                        )),
                    Center(
                        child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        this._displayText,
                        style: textStyle,
                      ),
                    ))
                  ],
                ))));
  }

  String calculateTotalReturn() {
    double principal = double.parse(principalController.text);
    double roi = double.parse(roiController.text);
    double term = double.parse(termController.text);

    double result = principal + principal * roi / 100 * term;

    return "After $term years your investment will be worth $result $_selectedCurrency";
  }

  void resetValues() {
    this.principalController.text = "";
    this.roiController.text = "";
    this.termController.text = "";

    this._selectedCurrency = this._currencies[0];
  }
}
